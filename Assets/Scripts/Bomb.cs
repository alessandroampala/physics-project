using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private float explosionRange = 5;
    [SerializeField] private float explosionForce = 5;
    [SerializeField] private float perJointDamage = 1;
    [SerializeField] private float timeout = 3;

    [Header("Color")]
    [SerializeField] private Color aColor, bColor;
    [HideInInspector] public float t = 0; //[0, 1]. Changed by animation
    Material material;

    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
        StartCoroutine(Countdown());
    }

    void Update()
    {
        material.color = Color.Lerp(aColor, bColor, t);
    }

    IEnumerator Countdown()
    {
        yield return new WaitForSeconds(timeout);
        Explode(transform.position);
        Destroy(gameObject);
    }

    void Explode(Vector3 position)
    {
        Collider[] colliders = Physics.OverlapSphere(position, explosionRange);
        foreach (Collider c in colliders)
        {
            JointScript joint = c.gameObject.GetComponent<JointScript>();
            joint?.JointDamage(perJointDamage);

            Vector3 force = (c.transform.position - position).normalized * explosionForce;
            Rigidbody rb = c.GetComponent<Rigidbody>();
            if(rb != null)
                rb.AddForce(force, ForceMode.Impulse);
        }
    }
}
