using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThrowObject : MonoBehaviour
{

    [SerializeField] private GameObject prefab;
    [SerializeField] private float force;
    [SerializeField] public float instantiationffset = 2; 

    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            GameObject g = Instantiate(prefab, mainCamera.transform.position + ray.direction * instantiationffset, Quaternion.identity);
            g.transform.rotation = Quaternion.LookRotation(ray.direction);
            Rigidbody rb = g.GetComponent<Rigidbody>();
            rb?.AddForce(ray.direction * force, ForceMode.Impulse);
        }
    }
}
