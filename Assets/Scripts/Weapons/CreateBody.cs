using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CreateBody : MonoBehaviour
{
    [SerializeField] GameObject bodyPrefab;
    List<GameObject> bodies = new List<GameObject>();
    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Plane p = new Plane(Vector3.forward, GameManager.instance.spawnPosition);
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            float enter = 0;

            if (p.Raycast(ray, out enter))
            {
                Vector3 finalPoint = mainCamera.ScreenToWorldPoint(Input.mousePosition) + ray.direction * enter;
                InstantiateBody(finalPoint);
            }
        }
    }

    public void InstantiateBody(Vector3 position)
    {
        GameObject g = Instantiate(bodyPrefab, position, Quaternion.identity);
        SkinnedMeshRenderer[] smrs = g.GetComponentsInChildren<SkinnedMeshRenderer>();
        SkinnedMeshRenderer smr = smrs[0].sharedMesh.name == "Body" ? smrs[0] : smrs[1];
        smr.material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        RegisterBody(g);
    }

    public void RegisterBody(GameObject body)
    {
        bodies.Add(body);
    }
    public void ClearBodies()
    {
        bodies.ForEach(b => Destroy(b));
    }
}
