using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseShoot : MonoBehaviour
{
    [SerializeField] int damage = 2;
    [SerializeField] float forceCoeff = 1000f;
    [SerializeField] float shootRatio = 10;
    [SerializeField] GameObject blood;
    [SerializeField] AudioClip shootSound;
    ParticleSystem bloodParticles;
    Camera mainCamera;
    float lastShootTime = 0;
    float timeBetweenShoot;

    Dictionary<ConfigurableJoint, Rigidbody> jointDict = new Dictionary<ConfigurableJoint, Rigidbody>();
    List<GameObject> bodies = new List<GameObject>();

    GameObject lockedObj;
    AudioSource audioSource;


    void Start()
    {
        blood = Instantiate(blood);
        bloodParticles = blood.GetComponent<ParticleSystem>();
        timeBetweenShoot = 1f / shootRatio;
        mainCamera = Camera.main;
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = shootSound;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && Time.time > lastShootTime + timeBetweenShoot)
        {
            Shoot();
            lastShootTime = Time.time;
        }
    }

    void Shoot()
    {
        RaycastHit hit;
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.transform.name);

            JointScript joint = hit.transform.GetComponent<JointScript>();
            if (joint == null) return;
            joint.JointDamage(damage);
            
            Vector3 force = (hit.point - mainCamera.transform.position).normalized * forceCoeff;
            hit.rigidbody.AddForceAtPosition(force, hit.point, ForceMode.Impulse);

            blood.transform.position = hit.point;
            blood.transform.rotation = Quaternion.LookRotation(hit.normal);
            bloodParticles.Play();
            audioSource.Play();
        }
    }
}
