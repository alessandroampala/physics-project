using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MannequinCharacter : MonoBehaviour
{
    public float health = 100;

    private ConfigurableJoint[] joints;

    void Start()
    {
        joints = GetComponentsInChildren<ConfigurableJoint>();
    }

/*    void Update()
    {
        
    }*/

    private void Die()
    {
        foreach(ConfigurableJoint joint in joints)
        {
            JointDrive drive = new JointDrive();
            drive.positionSpring = 0;

            joint.angularXDrive = drive;
            joint.angularYZDrive = drive;
        }
    }

    public void GetDamage(float damage)
    {
        if (health < 0) return;

        health -= damage;
        if (health < 0)
            Die();
    }
}
