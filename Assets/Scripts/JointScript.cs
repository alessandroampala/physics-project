using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointScript : MonoBehaviour
{
    float jointHealth = 10;

    ConfigurableJoint joint;

    //Audio
    const int soundMagnitudeThreshold = 2;
    const float dontPlayUnderSec = 1f;
    AudioSource m_audioSource;
    float playedAtTime = 0;

    //Damage
    MannequinCharacter character;
    const int damageMagnitudeThreshold = 5;    
    const int damagePerCollision = 3;    

    void Start()
    {
        joint = GetComponent<ConfigurableJoint>();
        character = transform.root.GetComponent<MannequinCharacter>();
        m_audioSource = transform.root.GetComponent<AudioSource>();
    }

    #region CollisionRegion

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.relativeVelocity.magnitude);

        PlaySound(collision);
        CollisionDamage(collision);
    }

    private void CollisionDamage(Collision collision)
    {
        if (collision.transform.root != transform.root && collision.relativeVelocity.magnitude > damageMagnitudeThreshold)
        {
            character.GetDamage(damagePerCollision);
        }
    }

    private void PlaySound(Collision collision)
    {
        if (Time.time > playedAtTime + dontPlayUnderSec)
        {
            playedAtTime = Time.time;
            if (collision.transform.root != transform.root && collision.relativeVelocity.magnitude > soundMagnitudeThreshold)
            {
                m_audioSource.clip = GameManager.instance.hitClips[Random.Range(0, GameManager.instance.hitClips.Count)];
                m_audioSource.pitch = Random.Range(.8f, 1.2f);
                m_audioSource.Play();
            }
        }
    }

    #endregion
    
    public void JointDamage(float amount)
    {
        if (jointHealth < 0) return;

        character.GetDamage(amount);
        jointHealth -= amount;
        if(jointHealth <= 0)
        {
            DetachJoint(joint);
        }
    }

    void DetachJoint(ConfigurableJoint joint)
    {
        if (!joint.transform.name.Contains("Spine") &&
            !joint.transform.name.Equals("Hips") &&
            !joint.transform.name.Contains("Shoulder"))
        {
            joint.connectedBody = null;
            joint.xMotion = ConfigurableJointMotion.Free;
            joint.yMotion = ConfigurableJointMotion.Free;
            joint.zMotion = ConfigurableJointMotion.Free;

            JointDrive drive = new JointDrive();
            drive.positionSpring = 0;
            joint.angularXDrive = drive;
            joint.angularYZDrive = drive;
        }
    }
}
