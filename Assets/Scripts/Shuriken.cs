using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shuriken : MonoBehaviour
{
    [SerializeField] float damage = 5f;
    [SerializeField] float rotationSpeed = 5f;

    BoxCollider boxCollider;
    Rigidbody rb;
    
    void Start()
    {
        boxCollider = GetComponentInChildren<BoxCollider>();
        rb = GetComponent<Rigidbody>();
        rb.AddTorque(Vector3.right * rotationSpeed, ForceMode.Impulse);
    }

    void OnCollisionEnter(Collision collision)
    {
        JointScript joint = collision.transform.GetComponent<JointScript>();
        joint?.JointDamage(damage);

        transform.SetParent(collision.transform);
        Destroy(boxCollider);
        rb.useGravity = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        transform.position = collision.contacts[0].point;
        rb.isKinematic = true;
        Destroy(this);
    }
}
