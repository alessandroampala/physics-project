using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchLimbRotation : MonoBehaviour
{
    [SerializeField] private Transform m_targetLimb;
    private ConfigurableJoint m_configurableJoint;
    private Quaternion m_targetInitialRotation;

    void Start()
    {
        m_configurableJoint = GetComponent<ConfigurableJoint>();
        m_targetInitialRotation = m_targetLimb.transform.localRotation;
    }

    void FixedUpdate()
    {
        m_configurableJoint.targetRotation = CalculateTargetRotation();
    }

    private Quaternion CalculateTargetRotation()
    {
        //calculate offset from initial rotation
        //targetRotation = currentRotation - initialRotation
        return Quaternion.Inverse(m_targetLimb.localRotation) * m_targetInitialRotation;
    }
}
