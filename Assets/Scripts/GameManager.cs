using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public List<AudioClip> hitClips;
    public readonly Vector3 spawnPosition = new Vector3(0, 1f, 0);

    [Header("Weapons")]
    [SerializeField] private List<MonoBehaviour> weapons;
    [Header("Weapon Hotbar Buttons")]
    [SerializeField] private List<Button> weaponsButtons;
    [SerializeField] private Color selectedColor;
    [SerializeField] private Color deselectedColor;

    void Awake()
    {
        if(instance == null)
            instance = this;

#if UNITY_EDITOR
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;
#endif
    }

    void Start()
    {
        weaponsButtons[0].onClick.Invoke();   
        GetComponent<CreateBody>().InstantiateBody(spawnPosition);
    }

    public void SelectWeapon(MonoBehaviour weaponComponent)
    {
        weaponsButtons.ForEach(b =>
        {
            ColorBlock colorBlock = ColorBlock.defaultColorBlock;
            colorBlock.normalColor = deselectedColor;
            b.colors = colorBlock;
        });
        weapons.ForEach(w => w.enabled = false);
        weaponComponent.enabled = true;
    }

    public void SelectButton(Button b)
    {
        ColorBlock colorBlock = ColorBlock.defaultColorBlock;
        colorBlock.normalColor = selectedColor;
        b.colors = colorBlock;
    }
}
